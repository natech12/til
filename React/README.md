### setStateの非同期更新対策
propsとstateは非同期に更新されるため、それらの値を使った更新は関数で行う。
```
// 直前のstateとstate更新適用時のpropsの値を引数に受け取る
this.setState((state, props) => ({
  counter: state.counter + props.increment
}));
```

### イベントハンドラの記載について
以下のように、イベントハンドラにアロー関数を指定すると、ボタンがレンダリングされるたびに新しい関数が作成される。
通常は問題ないが、アロー関数を子コンポーネントへのpropsに指定した場合は、子コンポーネントが常に再描画されてしまう。
```
<button onClick={() => this.handleClick()}>
  クリック
</button>
```

### 制御されたコンポーネント
inputタグなどは、通常タグ自身が入力された文字列などの状態を保持している。
これをReactのstateに状態を委任することで、stateを唯一の情報源にすることができる。
このようにReactによって値が制御されるコンポーネントを「制御されたコンポーネント」と呼ぶ。

### コンテキスト
通常Reactコンポーネント間のデータ受け渡しはpropsを用いてトップダウンで渡される。
ただし各コンポーネントで共通的に使用する値については、propsよりコンテキストで共有した方がデータの受け渡しが楽になる。

※コンテキストに依存したコンポーネントは再利用性が低下するため、使用用途は最小限に留める。

```
const ThemeContext = React.createContext('light');  // コンテキストの作成と初期値の設定

class App extends React.Component {
  render() {
    return (
      <ThemeContext.Provider>  // 子コンポーネントにコンテキストの値を渡す
        <Header />
        <ThemeContext.Provider value="dark">  // 初期値から値を変更して子コンポーネントに渡すことも可能
          <Main />
        </ThemeContext.Provider>
      </ThemeContext.Provider>
    );
  }
}

class Main extends React.Component {
  static contextType = ThemeContext;  // 使用するコンテキストをcontextTypeに設定する
  render() {
    return <Button theme={this.context} />;  // this.contextで値を取得する
  }
}

// 関数コンポーネントで購読する場合は以下のように書く
<ThemeContext.Consumer>
  {value => return value}
</ThemeContext.Consumer>
```